import subprocess
import os
import sys
import time
import getpass
import json
# from time import gmtime, strftime, localtime


def Logger(string):
    # print("%s : %s" % (strftime("%Y-%m-%d %H:%M:%S", localtime()), string))
    print(string)
    sys.stdout.flush()


def callProcess(
    cmd,
    live_output=False,
    printcmd=False,
    curdir="/",
    valid_returncodes=[0, ],
    root=False
):
    output = ""

    if printcmd:
        Logger(cmd)

    if root and getpass.getuser() != "root":
        cmd = "sudo " + cmd

    process = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        shell=True, cwd=curdir, executable='/bin/bash'
    )

    if live_output:
        for line in iter(process.stdout.readline, ''):
            if len(line) > 0:
                Logger(line.decode("ascii", "ignore").rstrip("\n"))
                output = output + line.decode("ascii", "ignore")
            else:
                break

        process.communicate()
    else:
        data = process.communicate()
        output = data[0].decode("utf-8")

    if process.returncode not in valid_returncodes:
        raise subprocess.CalledProcessError(
            process.returncode, cmd=cmd, output=output
        )

    return output


class Monitor:
    def __init__(self, cmd):
        self.cmd = cmd
        self.strikes = 0
        self.pollCnt = 0

    def poll(self):
        output = callProcess(self.cmd)

        if output.strip("\n").isdigit():
            if int(output.strip("\n")) == 0:
                self.strikes += 1
            else:
                self.strikes = 0


def main():
    Logger("#######################")
    Logger("### nas-idle.py ###")
    Logger("#######################")

    monitors = list()

    if not os.path.exists('/opt/nas-idle/nas-idle.json'):
        print("File /opt/nas-idle/nas-idle.json does not exist")
        sys.exit(1)

    try:
        with open('/opt/nas-idle/nas-idle.json') as f:
            monitors = json.load(f)
    except ValueError:
        print(
            (
                "Unable to load Invalid json file "
                "/opt/nas-idle/nas-idle.json"
            )
        )
        sys.exit(1)

    for monitor in monitors:
        monitor["monitor"] = Monitor(monitor["cmd"])

    while True:
        for monitor in monitors:
            monitor["monitor"].pollCnt += 1

            if monitor["monitor"].pollCnt == monitor["freq"]:
                monitor["monitor"].poll()

                monitor["monitor"].pollCnt = 0

                if monitor["monitor"].strikes >= monitor["strikes"]:
                    Logger(
                        (
                            f"monitor [{monitor['name']}], "
                            f"[{monitor['monitor'].strikes}] strikes, IDLE"
                        )
                    )
                else:
                    Logger(
                        (
                            f"monitor [{monitor['name']}], "
                            f"[{monitor['monitor'].strikes}] strikes"
                        )
                    )

        idleCnt = len(
            [x for x in monitors if x["monitor"].strikes >= x["strikes"]]
        )

        if idleCnt == len(monitors):
            Logger("Idle, shutdown")
            callProcess("shutdown -P now", root=True)

        time.sleep(1)


if __name__ == "__main__":
    main()
