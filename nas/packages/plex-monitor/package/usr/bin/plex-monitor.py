import time
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
import json
import subprocess
import getpass


class ActionHander:
    def __init__(self, config):
        self.config = config

    def __call__(self, event):
        print(f"Event {event}")
        callProcess(self.config["action"], live_output=True, printcmd=True)


def callProcess(
    cmd, live_output=False, printcmd=False, curdir="/",
    valid_returncodes=[0, ], root=False, inc_returncode=False
):
    output = ""

    if printcmd:
        print(cmd)

    if root and getpass.getuser() != "root":
        cmd = "sudo " + cmd

    process = subprocess.Popen(
        cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True,
        cwd=curdir
    )

    if live_output:
        for line in iter(process.stdout.readline, ''):
            if len(line) > 0:
                print(line.decode("ascii", "ignore").rstrip("\n"))
                output = output + line.decode("ascii", "ignore")
            else:
                break

        process.communicate()
    else:
        data = process.communicate()
        output = data[0].decode("utf-8")

    if process.returncode not in valid_returncodes:
        raise subprocess.CalledProcessError(process.returncode, cmd=cmd, output=output)

    if inc_returncode:
        return (process.returncode, output)
    else:
        return output


def run():
    with open("/opt/plex-monitor/plex-monitor.json", "r") as f:
        config = json.load(f)

    observer = Observer()

    for monitor in config:
        event_handler = PatternMatchingEventHandler("*", "", False, True)
        action_handler = ActionHander(monitor)

        event_handler.on_created = action_handler
        event_handler.on_deleted = action_handler
        observer.schedule(event_handler, monitor["directory"], recursive=True)

    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()


if __name__ == "__main__":
    run()
